import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  auth_status: boolean;

  constructor(private authService: AuthService, private router: Router) {}

  ngOnInit(): void {
    this.auth_status = this.authService.is_auth;
  }

  on_sign_in() {
    this.authService.sign_in().then(
      () => {
        this.auth_status = this.authService.is_auth;
        this.router.navigate(['fruits']);
      }
    );
  }

  on_sign_out() {
    console.log('connexion down');
    this.authService.sign_out();
    this.auth_status = this.authService.is_auth;
  }

}
