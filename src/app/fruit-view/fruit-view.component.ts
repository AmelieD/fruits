import { Component, OnInit } from '@angular/core';
import { FruitService } from '../services/fruit.service';
import { Subscription } from "rxjs/Subscription";

@Component({
  selector: 'app-fruit-view',
  templateUrl: './fruit-view.component.html',
  styleUrls: ['./fruit-view.component.css']
})
export class FruitViewComponent implements OnInit {
  is_auth = false;

  last_update = new Promise(
    (resolve, reject) => {
      const date = new Date();
      setTimeout(() => {
        resolve(date);
      }, 2000);
    }
  );

  fruits: any[];
  // niveau d'abstration des données
  fruit_subscription: Subscription;

  constructor(private fruitService: FruitService) {
    setTimeout (() => {
      this.is_auth = true;
    }, 2000);
  }

  ngOnInit() {
    this.fruit_subscription = this.fruitService.fruit_subject.subscribe(
      (fruits: any[]) => {
        this.fruits = fruits;
      }
    );
    this.fruitService.emit_fruit_subject();
  }
  enable() {
    this.fruitService.switch_on_all();
  }

  disable() {
    this.fruitService.switch_off_all();
  }

  on_save() {
    this.fruitService.save_fruit_to_server();
  }

  on_fetch() {
    this.fruitService.get_fruit_from_server();
  }
}
