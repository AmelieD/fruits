import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FruitViewComponent } from './fruit-view.component';

describe('FruitViewComponent', () => {
  let component: FruitViewComponent;
  let fixture: ComponentFixture<FruitViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FruitViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FruitViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
