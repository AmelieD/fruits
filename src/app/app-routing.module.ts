import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthComponent } from './auth/auth.component';
import { FruitViewComponent } from './fruit-view/fruit-view.component';
import { SingleFruitComponent } from './single-fruit/single-fruit.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { AuthGuardService } from './services/auth-guard.service';
import { EditFruitComponent } from './edit-fruit/edit-fruit.component';
import { UserListComponent } from './user-list/user-list.component';
import { NewUserComponent } from './new-user/new-user.component';


const routes: Routes = [
  { path: 'fruits', canActivate: [AuthGuardService],component: FruitViewComponent },
  { path: 'fruits/:id', canActivate: [AuthGuardService], component: SingleFruitComponent },
  { path: 'edit', canActivate: [AuthGuardService], component: EditFruitComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'users', component: UserListComponent },
  { path: 'new_user', component: NewUserComponent },
  { path: 'not_found', component: FourOhFourComponent },
  // put this path at the end of routes
  { path: '**', redirectTo: '/not_found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
