import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from "../models/User.model";
import { Subscription } from "rxjs/Subscription";
import { UserService } from "../services/user.service";

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit, OnDestroy {

  users: User[];
  user_subscription: Subscription;
  constructor(private userService: UserService) { }

  ngOnInit(): void {
    this.user_subscription = this.userService.user_subject.subscribe(
      (users: User[]) => {
        this.users = users;
      }
    );
    this.userService.emit_users();
  }

  ngOnDestroy() {
    this.user_subscription.unsubscribe();
  }
}
