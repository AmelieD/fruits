import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FoodComponent } from './food/food.component';
import { FruitComponent } from './fruit/fruit.component';
import { FruitService } from './services/fruit.service';
import { AuthService } from './services/auth.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AuthComponent } from './auth/auth.component';
import { FruitViewComponent } from './fruit-view/fruit-view.component';
import { SingleFruitComponent } from './single-fruit/single-fruit.component';
import { FourOhFourComponent } from './four-oh-four/four-oh-four.component';
import { EditFruitComponent } from './edit-fruit/edit-fruit.component';
import { UserListComponent } from './user-list/user-list.component';
import { UserService } from './services/user.service';
import { NewUserComponent } from './new-user/new-user.component';


@NgModule({
  declarations: [
    AppComponent,
    FoodComponent,
    FruitComponent,
    AuthComponent,
    FruitViewComponent,
    SingleFruitComponent,
    FourOhFourComponent,
    EditFruitComponent,
    UserListComponent,
    NewUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [
    FruitService,
    AuthService,
    AuthGuardService,
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
