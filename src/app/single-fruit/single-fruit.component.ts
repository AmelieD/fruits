import { Component, OnInit } from '@angular/core';
import { FruitService } from '../services/fruit.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-single-fruit',
  templateUrl: './single-fruit.component.html',
  styleUrls: ['./single-fruit.component.css']
})
export class SingleFruitComponent implements OnInit {
  name: string = 'Fruit';
  category: string = 'category';

  constructor(private fruitService: FruitService,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.name = this.fruitService.get_fruit_by_id(+id).name;
    this.category = this.fruitService.get_fruit_by_id(+id).category;
  }

}
