import { Component, Input, OnInit } from '@angular/core';
import { FruitService } from '../services/fruit.service';


@Component({
  selector: 'app-fruit',
  templateUrl: './fruit.component.html',
  styleUrls: ['./fruit.component.css']
})
export class FruitComponent implements OnInit {
  @Input() fruit_name: string;
  @Input() fruit_category: string;
  @Input() index_of_fruit: number;
  @Input() id: number;
  constructor(private fruitService: FruitService) { }

  ngOnInit(): void {
  }

  get_status() {
    return this.fruit_category;
  }

  get_color() {
    if(this.fruit_category === 'Acid') {

      console.log(this.fruit_category);
      return 'red';
    }
  }

  on_switch_on() {
    this.fruitService.switch_on_1(this.index_of_fruit);
  }

  on_switch_off() {
    this.fruitService.switch_off_1(this.index_of_fruit);
  }
}
