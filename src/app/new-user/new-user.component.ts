import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UserService } from '../services/user.service';
import { Router } from "@angular/router";
import { User } from "../models/User.model";

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.css']
})
export class NewUserComponent implements OnInit {

  user_form: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private userService: UserService,
              private router: Router) { }

  ngOnInit(): void {
    this.init_form();
  }

  init_form() {
    this.user_form = this.formBuilder.group({
      first_name: ['', Validators.required],
      last_name: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      drink_preference: ['', Validators.required],
      hobbies: this.formBuilder.array([])
    });
  }

  on_submit_form() {
    const form_value = this.user_form.value;
    const new_user = new User(
      form_value['first_name'],
      form_value['last_name'],
      form_value['email'],
      form_value['drink_preference'],
      form_value['hobbies'] ? form_value['hobbies'] : []
    );
    this.userService.add_user(new_user);
    this.router.navigate(['/users']);
  }

  get_hobbies() {
    return this.user_form.get('hobbies') as FormArray;
  }

  on_add_hobby() {
    const new_hobby_control = this.formBuilder.control('', Validators.required);
    this.get_hobbies().push(new_hobby_control);
  }

}
