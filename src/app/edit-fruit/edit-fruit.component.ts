import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { FruitService } from "../services/fruit.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-edit-fruit',
  templateUrl: './edit-fruit.component.html',
  styleUrls: ['./edit-fruit.component.css']
})
export class EditFruitComponent implements OnInit {
  default_on_acid = 'Acid';

  constructor(private fruitService: FruitService,
              private router: Router) { }

  ngOnInit(): void {
  }

  on_submit(form: NgForm) {
    const name     = form.value['name'];
    const category = form.value['category'];
    this.fruitService.add_fruit(name, category);
    this.router.navigate(['/fruits']);
  }
}
