import { Subject } from "rxjs/Subject";
import { HttpClient } from '@angular/common/http';
import {Injectable} from "@angular/core";

@Injectable()
export class FruitService {
  fruit_subject = new Subject<any[]>();
  // private provide access for external
  // niveau d'abstration des données
  private fruits = [
    // {
    //   id: 1,
    //   name: 'Citron',
    //   category: 'Acid'
    // },
    // {
    //   id: 2,
    //   name: 'Orange',
    //   category: 'Doux'
    // },
    // {
    //   id: 3,
    //   name: 'Ananas',
    //   category: 'Acid'
    // }
  ];

  constructor(private httpClient: HttpClient) {}

  // copie de la liste des fruits, accessible depuis ext
  emit_fruit_subject() {
    this.fruit_subject.next(this.fruits.slice());
  }

  get_fruit_by_id(id: number) {
    const fruit = this.fruits.find(
      (fruit_object) => {
        return fruit_object.id;
      }
    );
    return fruit;
  }

  switch_on_all() {
    for(let fruit of this.fruits) {
      fruit.category = 'Doux'
    }
    this.emit_fruit_subject();
  }

  switch_off_all() {
    for(let fruit of this.fruits) {
      fruit.category = 'Acid'
    }
    this.emit_fruit_subject();
  }

  switch_on_1(index: number) {
    this.fruits[index].category = 'Doux'
    this.emit_fruit_subject();
  }

  switch_off_1(index: number) {
    this.fruits[index].category = 'Acid'
    this.emit_fruit_subject();
  }

  add_fruit(name: string, category: string) {
    const fruit_object = {
      id: 0,
      name: '',
      category: ''
    };
    fruit_object.name     = name;
    fruit_object.category = category;
    fruit_object.id       = this.fruits[(this.fruits.length - 1)].id + 1;

    this.fruits.push(fruit_object);
    this.emit_fruit_subject();
  }

  save_fruit_to_server() {
    this.httpClient
      .put("https://http-client-foody.firebaseio.com/fruits.json", this.fruits)
      .subscribe(
        () => {
          console.log('ok');
        },
        (error) => {
          console.log(error);
        }
      )
  }

  get_fruit_from_server() {
    this.httpClient
      .get<any[]>("https://http-client-foody.firebaseio.com/fruits.json")
      .subscribe(
        (response) => {
          this.fruits = response;
          this.emit_fruit_subject();
        },
        (error) => {
          console.log(error);
        }
      )
  }
}
