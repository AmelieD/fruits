import { User } from "../models/User.model";
import { Subject } from "rxjs/Subject";


export class UserService {
  private users: User[] = [
    {
      first_name: 'John',
      last_name: 'Doe',
      email: 'john.doe@gmail.com',
      drink_preference: 'coca',
      hobbies: [
        'coder',
        'badminton'
      ]
    }
  ];
  user_subject = new Subject<User[]>();

  emit_users() {
    this.user_subject.next(this.users.slice());
  }

  add_user(user: User) {
    this.users.push(user);
    this.emit_users();
  }
}
