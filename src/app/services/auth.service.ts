export class AuthService {
  is_auth = false;

  sign_in() {
    return new Promise(
      (resolve, reject) => {
        setTimeout(() => {
          this.is_auth = true;
          resolve(true);
        }, 2000);
      }
    );
  }

  sign_out() {
    this.is_auth = false;
  }
}
